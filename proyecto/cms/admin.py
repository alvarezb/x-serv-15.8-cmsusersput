from django.contrib import admin

from .models import Contenido,Comentario

# Register your models here.
#Aqui registramos el modelo
admin.site.register(Contenido)
admin.site.register(Comentario)