from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect

from .models import Contenido

# Create your views here

formulario = """
No existe valor en la base de datos para esta llave
<p>Introdúcela: </p>
<form action="" method="POST">
    Valor: <input type="text" name="valor">
    <br/><input type="submit" value="Enviar">
</form>
"""

#Lee de la base de datos,decorador:
@csrf_exempt
def get_content(request, llave):
    # Si es PUT:
    if request.method in ["POST","PUT"]:
        if request.method == "POST":
            valor = request.POST['valor']
        else:
            valor = request.body.decode('utf-8')
        try:
            contenido = Contenido.objects.get(clave=llave)
            contenido.delete()
        except Contenido.DoesNotExist:
            pass
        contenido = Contenido(clave=llave, valor=valor)
        contenido.save()
    try:
        contenido = Contenido.objects.get(clave=llave)
        respuesta = "El valor de la llave es: " + contenido.valor
    except Contenido.DoesNotExist:
        if request.user.is_authenticated:
             respuesta = formulario
        else:
            respuesta = "No estas autenticado. <a href='/login'>Haz login!</a>"

    return HttpResponse(respuesta)

def index(request):
    #1.Tener la lista de contenidos
    content_list = Contenido.objects.all()
    #2.Cargar la plantilla
    template = loader.get_template('cms/index.html')
    #3. Ligar las variables de la plantilla a la variable de python
    context = {
        'content_list': content_list
    }
    return HttpResponse(template.render(context, request))

def loggedIn(request):
    if request.user.is_authenticated:
        respuesta = "Logged in as " + request.user.username
    else:
        respuesta = "No estas autenticado, <a href='/login'>Autenticate</a>"

    return HttpResponse(respuesta)

def logout_view(request):
    logout(request)
    return redirect("/cms/")

def imagen(request):
    template = loader.get_template('cms/plantilla.html') #Cargo la plantilla
    context = {}
    return HttpResponse(template.render(context,request))